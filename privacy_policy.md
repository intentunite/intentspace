Privacy Policy for IntentSpace

Effective Date: 03/01/2024

1. Introduction
User Jakub Pawlików ("we", "us", "our") has developed IntentSpace as a free service ("Service"). This privacy policy ("Policy") sets forth our policies regarding the collection, use, and disclosure of information about you in connection with your use of IntentSpace.

2. Information Collection and Use
We do not collect personal information from our users. Users can create and share content (intentions) anonymously. These intentions are assigned random hashes and do not contain personally identifiable information.

3. Content
User-generated content is publicly available. If content is not updated within one year period, it may be automatically deleted.

4. Editing and Deletion of Content
Users have the ability to edit or delete their intentions from the device where they were created. This action is irreversible, and once deleted, the content cannot be recovered.

5. Data Security
We use Firebase App Check to enhance the security of our app and protect against unauthorized access.

6. Children's Privacy
IntentSpace does not knowingly collect personal information from children under the age of 13. If we learn that we have collected personal information from a child under 13, we will take steps to delete such information as soon as possible.

7. International Use
Our Service is intended for global use. We strive to comply with all local laws and regulations regarding data protection and user privacy.

8. Analytics and Tracking
We do not track or analyze specific user behavior within the app. Our Service does not create user profiles or accounts.

9. Changes to This Privacy Policy
We may update this Privacy Policy periodically. We will notify you of any significant changes by posting the new Privacy Policy on the IntentSpace platform. We encourage you to review this Privacy Policy periodically.

10. Contact Us
If you have any questions about this Privacy Policy, please contact us at intent.space.team@gmail.com .
